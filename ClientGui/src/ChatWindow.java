import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import java.awt.event.KeyAdapter;
import javax.swing.JLabel;

import com.google.gson.Gson;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;


public class ChatWindow {

	private JFrame frame;
	private static JTextField textField;
	public static String SERVER_HOSTNAME = null;
	public static int SERVER_PORT = 0;
	public static BufferedReader in = null;
	public static PrintWriter out = null;
	public static ClientSender clientSender = null;
	public static ClientListener clientListener = null;
	static JTextArea textPane = null;
	static JTextArea textPane2 = null;
	private static Gson gson = new Gson();
	private static JLabel lblConnectedTo;
	public static JLabel lblChattingWith;
	private static String commandData = "none";
	private static int currentCommand = 101;
	/**
	 * Launch the application.
	 */
	public static void main(String username, String password, String hostname, int port) {
		
		try {
			
			ChatWindow window = new ChatWindow();
			window.frame.setVisible(true);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		SERVER_HOSTNAME = hostname;
		SERVER_PORT = port;
		startClient(username, password);
			
	}
	public static void startClient(String username, String password)
	{
		try {
			// Connect to Server
			addChat("Attempting connection to server " + SERVER_HOSTNAME + ":" + SERVER_PORT);
			Socket socket = new Socket(SERVER_HOSTNAME, SERVER_PORT);
			in = new BufferedReader( new InputStreamReader(socket.getInputStream()));
			out = new PrintWriter( new OutputStreamWriter(socket.getOutputStream()));
			addChat("Connected to server " + SERVER_HOSTNAME + ":" + SERVER_PORT);
			lblConnectedTo.setText("Connected to server " + SERVER_HOSTNAME + ":" + SERVER_PORT);
		} catch (IOException ioe) {
			addChat("Can not establish connection to " + SERVER_HOSTNAME + ":" + SERVER_PORT);
			ioe.printStackTrace();
		}

		// Create and start Sender thread
		clientSender = new ClientSender(out);
		clientSender.setDaemon(true);
		clientSender.start();
		login(username, password);
		
		clientListener = new ClientListener(in);
		clientListener.setDaemon(true);
		clientListener.start();
		
		
	}
	public static void login(String username, String password)
	{
		Command output = new Command(301, password, username);
		String json = gson.toJson(output);
		clientSender.sendMessage(json);
		
	}
	public static void addChat(String message)
	{
		textPane.setText(textPane.getText()+message+"\n");
		textPane.setCaretPosition(textPane.getDocument().getLength());
	}

	/**
	 * Create the application.
	 */
	public ChatWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 918, 551);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER)
				{
					Command output = new Command(currentCommand, commandData, textField.getText());
					String json = gson.toJson(output);
					clientSender.sendMessage(json);
	                textField.setText("");
				}
			}
		});
		textField.setBounds(21, 457, 597, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("Send");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Command output = new Command(currentCommand, commandData, textField.getText());
				String json = gson.toJson(output);
				clientSender.sendMessage(json);
                textField.setText("");
			}
		});
		btnNewButton.setBounds(628, 456, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		textPane = new JTextArea();
		textPane.setEditable(false);
		textPane.setBounds(21, 73, 696, 344);
		textPane.setLineWrap(true);
		textPane.setWrapStyleWord(true);
		textPane.setAutoscrolls(true);
		JScrollPane sp = new JScrollPane(textPane);
		sp.setSize(696, 360);
		sp.setLocation(21, 52);
		frame.getContentPane().add(sp);
		
		textPane2 = new JTextArea();
		textPane2.setEditable(false);
		textPane2.setBounds(21, 73, 696, 344);
		textPane2.setLineWrap(true);
		textPane2.setWrapStyleWord(true);
		textPane2.setAutoscrolls(true);
		JScrollPane sp2 = new JScrollPane(textPane2);
		sp2.setSize(165, 360);
		sp2.setLocation(727, 52);
		frame.getContentPane().add(sp2);
		
		lblConnectedTo = new JLabel("Connected To:");
		lblConnectedTo.setBounds(21, 11, 236, 14);
		frame.getContentPane().add(lblConnectedTo);
		
		lblChattingWith = new JLabel("Chatting With: "+commandData);
		lblChattingWith.setBounds(310, 11, 256, 14);
		frame.getContentPane().add(lblChattingWith);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnOptions = new JMenu("Options");
		menuBar.add(mnOptions);
		
		JMenuItem mntmOptions = new JMenuItem("Options");
		mnOptions.add(mntmOptions);
		
		JMenu mnCreateGroup = new JMenu("Chat With");
		menuBar.add(mnCreateGroup);
		
		JMenuItem mntmCreateGroup = new JMenuItem("User");
		mntmCreateGroup.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setTargetChat("david",103);
			}
		});
		mnCreateGroup.add(mntmCreateGroup);
		
		JMenuItem mntmJoinGroup = new JMenuItem("Group");
		mntmJoinGroup.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setTargetChat("Group 1",102);
			}
		});
		mnCreateGroup.add(mntmJoinGroup);
		
		JMenuItem mntmGlobal = new JMenuItem("Global");
		mntmGlobal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setTargetChat("Global",101);
			}
		});
		mnCreateGroup.add(mntmGlobal);
	}
	public void setTargetChat(String name, int command)
	{
		commandData = name;
		currentCommand = command;
		lblChattingWith.setText("Chatting With: "+ commandData);
	}
}
