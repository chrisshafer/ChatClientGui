import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;


class ClientSender extends Thread
{
	private PrintWriter out;

	public ClientSender(PrintWriter out)
	{
		this.out = out;
	}
	public void sendMessage(String message)
	{
		out.println(message);
	}
	//Reads messages from the console and sends them to the server
	public void run()
	{

		while (!isInterrupted()) {
			
			out.flush();
		}
	}
}
