import java.io.BufferedReader;
import java.io.IOException;

public class ClientListener extends Thread {

		BufferedReader in;
		public ClientListener(BufferedReader in) {
			this.in = in; 
			// store parameter for later user
		}
		
		public void run() {
			while(true){
			String message = "";
			// Read messages from the server and print the messages recieved
			try {
				while ((message=in.readLine()) != null) 
					ChatWindow.addChat(message);
			} catch (IOException e) {
				e.printStackTrace();
			}
			}
		}

}
